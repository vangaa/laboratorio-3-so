CC = gcc
CFLAGS = -Wall
EXEC = test

VPATH = src
OBJDIR = build/obj

ALL: build/lector build/computo build/escritor

build/lector: $(addprefix $(OBJDIR)/, lector.o mainLector.o)
	$(CC) $(CFLAGS) $^ -o $@

build/computo: $(addprefix $(OBJDIR)/, computo.o mainComputo.o)
	$(CC) $(CFLAGS) $^ -o $@

## Dependencias

$(OBJDIR)/computo.o: computo.h
$(OBJDIR)/lector.o: lector.h
$(OBJDIR)/mainLector.o: lector.h computo.h
$(OBJDIR)/mainComputo.o: lector.h computo.h

## Codigo objeto

$(OBJDIR)/%.o: %.c | build/obj
	$(CC) $(CFLAGS) -c  $< -o $@

$(OBJDIR):
	mkdir -p build/obj

## Prueba(s)

prueba: $(addprefix $(OBJDIR)/, main_prueba.o lector.o)
	$(CC) $(CFLAGS) $^ -o $@ 

prueba2: $(addprefix $(OBJDIR)/, main_prueba2.o lector.o)
	$(CC) $(CFLAGS) $^ -o $@ 

## Archivar

archive: src/*.c src/*.h
	git archive master --format=zip > lab3_17832733k_18018294.zip 

clean:
	rm -f $(OBJDIR)/*.o
	rm -f build/$(EXEC)
