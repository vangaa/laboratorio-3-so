#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/md5.h>


//char *get_md5(const char *str, int length) {
char *get_md5(const char *str) {
    int n;
    int length = strlen(str);
    MD5_CTX c;
    unsigned char digest[16];
    char *out = (char*)malloc(33);

    MD5_Init(&c);

    while (length > 0) {
        if (length > 512) {
            MD5_Update(&c, str, 512);
        } else {
            MD5_Update(&c, str, length);
        }
        length -= 512;
        str += 512;
    }

    MD5_Final(digest, &c);

    for (n = 0; n < 16; ++n) {
        snprintf(&(out[n*2]), 16*2, "%02x", (unsigned int)digest[n]);
    }

    return out;
}

int main(int argc, char **argv) {
	char* str1 = malloc(33*sizeof(char));
	for(int i=0; i<10000;i++){
		char str[5];
		sprintf(str, "%04d",i);
		strcpy(str1,str);
		char *output = get_md5(str1);
    	//char *output = get_md5("1234");
    	printf("%04d:%s\n", i, output);
    	if(i%200==0){
	    	//sleep(1);
    	}
    	free(output);
    }
    return 0;
}