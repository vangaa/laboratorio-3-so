#include <unistd.h>
#include <stdio.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <stdlib.h>

#include "lector.h"

/**
 * argv[1]: numero de threads
 * argv[2]: identificador memoria tareas
 * argv[3]: identificador memoria solucion
 */

task* tareas;	// lista de tareas
solucion* sol;	// la solucion

int main(int argc, char* argv[])
{
	int nthreads;
	int segid_tareas;
	int segid_solucion;

	if (argc == 1)
	{
		fprintf(stderr, "%s: Faltan argumentos\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	
	sscanf(argv[1], "%d", &nthreads);
	sscanf(argv[2], "%d", &segid_tareas);
	sscanf(argv[3], "%d", &segid_solucion);

	if ( (tareas = shmat(segid_tareas, 0, 0)) == (void*) -1)
	{
		fprintf(stderr, "%s: No se pudo obtener memoria de las tareas\n", argv[0]);
		exit(EXIT_FAILURE); 
	}

	if ( (sol = shmat(segid_solucion, 0, 0)) == (void*) -1)
	{
		fprintf(stderr, "%s: No se pudo obtener memoria de la solucion\n", argv[0]);
		exit(EXIT_FAILURE); 
	}

	/* crear threads */


	exit(EXIT_SUCCESS);
}
