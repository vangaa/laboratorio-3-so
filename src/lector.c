#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/shm.h>
#include <pthread.h>

#include "lector.h"

void print_task(task tarea)
{
	printf( "hash:    %s\n"
			"lim_inf: %d\n"
			"lim_sup: %d\n", tarea.hash, tarea.lim_inf, tarea.lim_sup);
}

void task_init(task* tarea, const char* hash, short lim_inf, short lim_sup)
{
		strcpy(tarea->hash, hash);
		tarea->lim_inf = lim_inf;
		tarea->lim_sup = lim_sup; 
}

int shmem_tareas_init(task* lista, int length)
{
	int seg_id = shmget(IPC_PRIVATE, length*sizeof(task),
			IPC_CREAT | S_IWUSR | S_IRUSR | S_IROTH); //flags

	lista = (task*) shmat(seg_id, NULL, 0);

	return seg_id;
}

int shmem_solucion_init(solucion* sol)
{
	int seg_id = shmget(IPC_PRIVATE, sizeof(solucion), 
			IPC_CREAT | S_IWUSR | S_IROTH | S_IWOTH); //flags

	sol = (solucion*) shmat(seg_id, 0, 0);

	return seg_id;
}

int shmem_mutex_init(pthread_mutex_t* mutex)
{
	int seg_id = shmget(IPC_PRIVATE, 2*sizeof(pthread_mutex_t),
			IPC_CREAT | S_IWUSR | S_IRUSR | S_IROTH | S_IWOTH);

	mutex = (pthread_mutex_t*) shmat(seg_id, NULL, 0);

	pthread_mutex_init (mutex, NULL);	// mutex para tareas
	pthread_mutex_init (mutex+1, NULL);	// mutex para las soluciones

	return seg_id;
}
