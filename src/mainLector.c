#include <unistd.h>
#include <stdio.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <pthread.h>

#include "lector.h"

const char usage[] =
			"OPCIONES ENTRADA\n"
			"\n"
			"opciones:\n"
			"  -p <num>\n"
			"     Numero de procesos de computo a crear\n"
			"  -t <num>\n"
			"     Numero de hebras por proceso de computo\n"
			"  -g <num>\n"
			"     Granularidad, que indica en cuantos numero de rangos se \n"
			"     dividen las tareas.\n";

task* tareas;
solucion* soluciones;
int* indices;					// productor y consumidor
pthread_mutex_t* acceso_lista;	// un solo mutex para el acceso
pthread_cond_t* var_cond;		// dos variables de condicion: lleno y vacio

int main(int argc, char* argv[])
{
	int nthreads, nprocesos, granularidad;
	int seg_tasks, seg_sol;
	char opt;

	if (argc == 1)
	{
		printf("uso: %s %s", argv[0], usage);
		exit(EXIT_FAILURE);
	}
	else //parsear argumentos
	{
		while ((opt = getopt(argc, argv, ":p:t:g:")) != -1)
		{
			switch (opt)
			{
				case 'p':
					sscanf(optarg, "%d", &nprocesos);
					if (nprocesos <= 0) goto error;
					break;
				case 't':
					sscanf(optarg, "%d", &nthreads);
					if (nthreads <= 0) goto error;
					break;
				case 'g':
					sscanf(optarg, "%d", &granularidad);
					if (granularidad <= 0) goto error;
					break; 
			}
		}
	}
	goto OK;

	error:
	fprintf(stderr, "%s: Parametro malo en la opcion «%c»\n", argv[0], opt);
	exit(EXIT_FAILURE);

	OK:
	if (argc < 8)
	{
		fprintf(stderr, "%s: Faltan argumentos\n", argv[0]); 
		exit(EXIT_FAILURE);
	}

	//ok
	
	/* memoria compartida */

	seg_tasks = shmem_tareas_init(tareas, MAX_TAREAS);
	seg_sol = shmem_solucion_init(soluciones);
	
	/* Crear procesos */

	/* Eliminar memoria compartida */

	shmdt(tareas);
	shmdt(soluciones);

	shmctl(seg_tasks, IPC_RMID, NULL);
	shmctl(seg_sol, IPC_RMID, NULL);
	
	exit(EXIT_SUCCESS);
}
