#ifndef LECTOR
#define LECTOR

#include <pthread.h>
#define MAX_TAREAS 10

typedef struct
{
	char hash[32];
	short lim_inf;
	short lim_sup;
}task;

typedef struct
{
	char hash[32];
	short password;
}solucion;

void print_task(task tarea);
void task_init(task* tarea, const char* hash, short lim_inf, short lim_sup);

/**
 * Pide memoria compartida necesaria de tanaño sizeof(task)*length y se la 
 * asigna a lista. Esta memoria tiene los permisos de lectura y escritura para 
 * el usuario dueño del segmento y permisos de lectura para todos los demas.
 *
 * @param lista Es la lista a la que se le asigna memoria
 * @param length Es el largo de la lista
 *
 * @return El identificador del segmento.
 */
int shmem_tareas_init(task* lista, int length);

/**
 * Inicializa la memoria compartida para el valor de la solucion.
 *
 * @param sol Es el puntero a la solucion.
 *
 * @return El identificador del segmento.
 */
int shmem_solucion_init(solucion* sol);

/**
 * Reserva memoria compartida e inicializa el mutex para acceder a la lista de 
 * tareas.
 *
 * @param mutex Es el puntero al mutex
 *
 * @return El identificador del segmento de memoria.
 */
int shmem_mutex_init(pthread_mutex_t* mutex);
#endif
